<?php

use ACFComposer\ACFComposer;
use Flynt\Components;

add_action('Flynt/afterRegisterComponents', function () {
    ACFComposer::registerFieldGroup([
        'name' => 'pageComponents',
        'title' => 'Page Components',
        'style' => 'seamless',
        'fields' => [
            [
                'name' => 'pageComponents',
                'label' => __('Page Components', 'flynt'),
                'type' => 'flexible_content',
                'button_label' => __('Add Component', 'flynt'),
                'layouts' => [
                    //Components\BlockCollapse\getACFLayout(),
                    //Components\BlockImage\getACFLayout(),
                    //Components\BlockImageText\getACFLayout(),
                    //Components\BlockVideoOembed\getACFLayout(),
                    Components\BlockHero\getACFLayout(),
                    Components\BlockWysiwyg\getACFLayout(),
                    Components\BlockFeature\getACFLayout(),
                    Components\GridImageText\getACFLayout(),
                    Components\BlockTabs\getACFLayout(),
                    Components\BlockForm\getACFLayout(),
                    Components\PageHeader\getACFLayout(),
                    Components\TextImageRight\getACFLayout(),
                    Components\TextImageLeft\getACFLayout(),
                    //Components\ListComponents\getACFLayout(),
                    //Components\SliderImages\getACFLayout(),
                ]
            ]
        ],
        'location' => [
            [
                [
                    'param' => 'post_type',
                    'operator' => '!=',
                    'value' => 'post'
                ]
            ]
        ]
    ]);
});
