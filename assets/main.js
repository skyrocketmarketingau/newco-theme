import './scripts/publicPath'
import 'console-polyfill'
import 'normalize.css/normalize.css'
import './main.scss'
import $ from 'jquery'
import feather from 'feather-icons'
import "magnific-popup";

import 'swiper/swiper-bundle.css';
import Swiper, { Navigation, Pagination } from 'swiper';
Swiper.use([Navigation, Pagination]);

import installCE from 'document-register-element/pony'

window.jQuery = $

require('bootstrap')

window.lazySizesConfig = window.lazySizesConfig || {}
window.lazySizesConfig.preloadAfterLoad = true
require('lazysizes')

$(document).ready(function () {
  feather.replace({
    'stroke-width': 1
  });

  $('[href^="https://www.youtube.com/watch?v"]').magnificPopup({
    type: 'iframe'
  });

  var mySwiper = new Swiper('.swiper-container', {
    observer: true,
    observeParents: true,
    spaceBetween: 20,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  })
});

installCE(window, {
  type: 'force',
  noBuiltIn: true
});

function importAll (r) {
  r.keys().forEach(r)
}

importAll(require.context('../Components/', true, /\/script\.js$/))
