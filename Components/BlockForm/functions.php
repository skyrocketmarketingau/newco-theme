<?php

namespace Flynt\Components\BlockForm;

use Flynt\FieldVariables;

function getACFLayout()
{
    return [
        'name' => 'BlockForm',
        'label' => 'Block: Form',
        'sub_fields' => [
            [
                'label' => __('General', 'flynt'),
                'name' => 'generalTab',
                'type' => 'tab',
                'placement' => 'top',
                'endpoint' => 0,
            ],
            [
                'label' => __('Content', 'flynt'),
                'name' => 'contentHtml',
                'type' => 'wysiwyg',
                'delay' => 1,
                'media_upload' => 0,
            ],
            [
                'label' => __('Background Image', 'flynt'),
                'name' => 'bg_image',
                'type' => 'image',
                'media_upload' => 0,
            ],
            [
                'label' => __('Options', 'flynt'),
                'name' => 'optionsTab',
                'type' => 'tab',
                'placement' => 'top',
                'endpoint' => 0
            ],
            [
                'label' => __('Section ID', 'flynt'),
                'name' => 'section_id',
                'type' => 'text',
            ],
            [
                'label' => __('Highlight Color', 'flynt'),
                'name' => 'highlight_color',
                'type' => 'select',
                'choices' => array(
                    'highlight-success' => 'Green',
                    'highlight-danger'  => 'Red',
                    'highlight-info'    => 'Blue',
                    'highlight-warning' => 'Yellow',
                    'highlight-fuchsia' => 'Fuchsia',
                    'highlight-teal'    => 'Teal',
                    'highlight-indigo'  => 'Indigo',
                ),
                'allow_null' => 1,
            ],
        ]
    ];
}
