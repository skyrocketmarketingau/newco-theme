<?php

namespace Flynt\Components\BlockTabs;

use Flynt\FieldVariables;

function getACFLayout()
{
    return [
        'name' => 'BlockTabs',
        'label' => 'Block: Tabs',
        'sub_fields' => [
            [
                'label' => __('General', 'flynt'),
                'name' => 'generalTab',
                'type' => 'tab',
                'placement' => 'top',
                'endpoint' => 0,
            ],
            [
                'label' => __('Title', 'flynt'),
                'name' => 'titleHtml',
                'type' => 'wysiwyg',
                'delay' => 1,
                'media_upload' => 0,
            ],
            [
                'label' => __('Tabs', 'flynt'),
                'name' => 'tabs',
                'type' => 'repeater',
                'collapsed' => '',
                'layout' => 'block',
                'button_label' => 'Add',
                'sub_fields' => [
                    [
                        'label' => __('Title', 'flynt'),
                        'name' => 'title',
                        'type' => 'text',
                    ],
                    [
                        'label' => __('Slides', 'flynt'),
                        'name' => 'slides',
                        'type' => 'repeater',
                        'collapsed' => '',
                        'layout' => 'block',
                        'button_label' => 'Add',
                        'sub_fields' => [
                            [
                                'label' => __('Image', 'flynt'),
                                'name' => 'image',
                                'type' => 'image',
                                'preview_size' => 'medium',
                                'instructions' => __('Image-Format: JPG, PNG.', 'flynt'),
                                'mime_types' => 'jpg,jpeg,png',
                                'wrapper' => [
                                    'width' => 40
                                ],
                            ],
                            [
                                'label' => __('Content', 'flynt'),
                                'name' => 'contentHtml',
                                'type' => 'wysiwyg',
                                'tabs' => 'visual,text',
                                'media_upload' => 0,
                                'delay' => 1,
                                'wrapper' => [
                                    'width' => 60
                                ],
                            ]
                        ]
                    ],
                    [
                        'label' => __('Highlight Color', 'flynt'),
                        'name' => 'highlight_color',
                        'type' => 'select',
                        'choices' => array(
                            'highlight-success' => 'Green',
                            'highlight-danger'  => 'Red',
                            'highlight-info'    => 'Blue',
                            'highlight-warning' => 'Yellow',
                            'highlight-fuchsia' => 'Fuchsia',
                            'highlight-teal'    => 'Teal',
                            'highlight-indigo'  => 'Indigo',
                        ),
                        'allow_null' => 1,
                    ],
                ]
            ],
            [
                'label' => __('Options', 'flynt'),
                'name' => 'optionsTab',
                'type' => 'tab',
                'placement' => 'top',
                'endpoint' => 0
            ],
            [
                'label' => __('Section ID', 'flynt'),
                'name' => 'section_id',
                'type' => 'text',
            ],
        ]
    ];
}
